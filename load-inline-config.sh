#!/bin/sh

if [ -n "$NGINX_INLINE_CONFIG" ]
then
    echo "Importing inline config"
    echo "$NGINX_INLINE_CONFIG" > /etc/nginx/conf.d/default.conf
    cat /etc/nginx/conf.d/default.conf
fi


if [ -n "$NGINX_INLINE_CONFIG_TEMPLATE" ]
then
    echo "Importing inline config template"
    echo "$NGINX_INLINE_CONFIG_TEMPLATE" > /etc/nginx/templates/default.conf.template
    cat /etc/nginx/templates/default.conf.template
fi
